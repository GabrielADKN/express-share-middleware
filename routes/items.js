const express = require('express');
const router = new express.Router();
const ExpressError = require('../expressError');
const items = require('../fakeDb');

router.get('/', (req, res) => {
    return res.json({ items });
});

router.post('/', (req, res, next) => {
    try {
        const { name, price } = req.body;
        if (!name || !price) {
            throw new ExpressError('Name and price are required', 400);
        }
        const newItem = { name, price };
        items.push(newItem);
        return res.status(201).json({ item: newItem });
    } catch (err) {
        return next(err);
    }
});

router.get('/:name', (req, res, next) => {
    const name = req.params.name;
    const item = items.find(item => item.name === name);
    if (item) {
        return res.json({ item });
    } else {
        throw new ExpressError('Item not found', 404);
    }
});

router.patch('/:name', (req, res, next) => {
    try {
        const name = req.params.name;
        const { name: newName, price } = req.body;
        const item = items.find(item => item.name === name);
        if (!item) {
            throw new ExpressError('Item not found', 404);
        }
        if (newName) item.name = newName;
        if (price) item.price = price;
        return res.json({ item });
    } catch (err) {
        return next(err);
    }
});

router.delete('/:name', (req, res, next) => {
    const name = req.params.name;
    const index = items.findIndex(item => item.name === name);
    if (index > -1) {
        items.splice(index, 1);
        return res.json({ message: 'Item deleted' });
    } else {
        throw new ExpressError('Item not found', 404);
    }
});

module.exports = router;
