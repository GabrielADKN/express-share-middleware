process.env.NODE_ENV = "test";

const request = require("supertest");

const app = require("../app");
const items = require("../fakeDb");
const { describe } = require("node:test");

let newItem = {
    name: "test",
    price: 10
};

beforeEach(() => {
    items.push(newItem);
});

afterEach(() => {
    items.length = 0;
});

describe("GET /items", () => {
    test("Gets list of items", async () => {
        const res = await request(app).get("/items");
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual({ items });
    });
});

describe("GET /items/:name", () => {
    test("Gets item by name", async () => {
        const res = await request(app).get(`/items/${newItem.name}`);
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual({ item: newItem });
    });

    test("Responds with 404 if item is not found", async () => {
        const res = await request(app).get(`/items/notFound`);
        expect(res.statusCode).toBe(404);
    });
});

describe("POST /items", () => {
    test("Creates a new item", async () => {
        const res = await request(app)
            .post("/items")
            .send(
                {
                    name: "test2",
                    price: 20
                }
            );
        expect(res.statusCode).toBe(201);
        expect(res.body).toEqual({ item: { name: "test2", price: 20 } });
    });
});

describe("PATCH /items/:name", () => {
    test("Updates an item", async () => {
        const res = await request(app)
            .patch(`/items/${newItem.name}`)
            .send(
                {
                    name: "test2",
                    price: 20
                }
            );
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual({ item: { name: "test2", price: 20 } });
    });
});

describe("DELETE /items/:name", () => {
    test("Deletes an item", async () => {
        const res = await request(app).delete(`/items/${newItem.name}`);
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual({ message: "Item deleted" });
    });
})